﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Resources;

namespace Ctrl
{
    public enum pony_type { earthpony, pegasus, regular, unihorn, zebra };

    public class brony
    {
        public brony()
        {
            this.p_name = "anon";
            this.p_type = pony_type.regular;
            this.id_p_type = (Int32)p_type;
            this.isActive = false;
            this.p_color = define_color(this.p_type, this.isActive);
        }

        public brony(string _name, pony_type _type, bool _isActive)
        {
            this.p_name = _name;
            this.p_type = _type;
            this.id_p_type = (Int32)_type;
            this.isActive = _isActive;
            this.p_color = define_color(this.p_type, this.isActive);
        }

        public void change_status(bool _c)
        {
            this.isActive = _c;
            this.p_color = define_color(this.p_type, this.isActive);
        }

        //Цвета
        string pc_earthpony_online = "#FF82C341";
        string pc_earthpony_offline = "#B382C341";
        string pc_pegasus_online = "#FF83D2E0";
        string pc_pegasus_offline = "#B383D2E0";
        string pc_unihorn_online = "#FFF15758";
        string pc_unihorn_offline = "#B3F15758";
        string pc_zebra_online = "#FF5760AC";
        string pc_zebra_offline = "#B35760AC";
        string pc_regular_online = "#FFFFFFFF";
        string pc_regular_offline = "#B3FFFFFF";

        public string p_name { get; set; }

        public pony_type p_type { get; set; }

        public bool isActive { get; set; }

        public Int32 id_p_type { get; set; }

        public string p_color { get; set; }

        public Bitmap avatar { get; set; }

        private string define_color(pony_type _t, bool isA)
        {

            switch ((Int32)_t)
            {
                case 0:
                    if (isActive) return pc_earthpony_online;
                    else return pc_earthpony_offline;
                case 1:
                    if (isActive) return pc_pegasus_online;
                    else return pc_pegasus_offline;
                case 2:
                    if (isActive) return pc_regular_online;
                    else return pc_regular_online;
                case 3:
                    if (isActive) return pc_unihorn_online;
                    else return pc_unihorn_offline;
                case 4:
                    if (isActive) return pc_zebra_online;
                    else return pc_zebra_offline;
                default:
                    if (isActive) return pc_regular_online;
                    else return pc_regular_online;
            }
        }
    }
}
