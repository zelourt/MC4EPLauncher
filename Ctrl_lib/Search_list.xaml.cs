﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ctrl
{
    /// <summary>
    /// Логика взаимодействия для Search_list.xaml
    /// </summary>
    public partial class Search_list : UserControl
    {
        public Search_list()
        {
            InitializeComponent();
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }

    public class bronys : ObservableCollection<brony>
    {
        public bronys()
        {
            Add(new brony("Pinkie", pony_type.earthpony, true));
            Add(new brony("Twilight", pony_type.unihorn, true));
            Add(new brony("Applejack", pony_type.earthpony, false));
            Add(new brony("Rainbow Dash", pony_type.pegasus, true));
            Add(new brony("Fluttershy", pony_type.pegasus, false));
            Add(new brony("Rarity", pony_type.unihorn,false));
            Add(new brony("unihorn offline", pony_type.unihorn, false));
            Add(new brony("unihorn online", pony_type.unihorn, true));
            Add(new brony("pegasus offline", pony_type.pegasus, false));
            Add(new brony("pegasus online", pony_type.pegasus, true));
            Add(new brony("earthpony offline", pony_type.earthpony, false));
            Add(new brony("earthpony online", pony_type.earthpony, true));
            Add(new brony("regular offline", pony_type.regular, false));
            Add(new brony("regular online", pony_type.regular, true));
            Add(new brony("zebra offline", pony_type.zebra, false));
            Add(new brony("zebra online", pony_type.zebra, true));
        }
    }

    public class Brony_temlate_selector : DataTemplateSelector
    {
        public DataTemplate earthponyTemplate { get; set; }
        public DataTemplate unihornTemplate { get; set; }
        public DataTemplate pegasusTemplate { get; set; }
        public DataTemplate regularTemplate { get; set; }
        public DataTemplate zebraTemplate { get; set; }


        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            brony brother = (brony)item;

            switch (brother.id_p_type)
            {
                case 0:
                    return earthponyTemplate;
                case 1:
                    return pegasusTemplate;
                case 2:
                    return regularTemplate;
                case 3:
                    return unihornTemplate;
                case 4:
                    return zebraTemplate;
                default:
                    return regularTemplate;
            }
        }
    }
}
