﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Drawing;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ctrl
{
    /// <summary>
    /// Логика взаимодействия для UserInform.xaml
    /// </summary>
    public partial class UserInform : UserControl
    {
        UserInformation myInfo;

        public UserInform()
        {
            InitializeComponent();
            changeInfo();
        }

        public void changeInfo(string _n, string _a, BitmapImage _i)
        {
            myInfo = new UserInformation(_n, _a, _i);
            u_Ava.Source = myInfo.Ava;
            u_About.Text = myInfo.AboutMe;
            u_Nick.Content = myInfo.Nick; 
        }

        public void changeInfo()
        {
            myInfo = new UserInformation();
            u_Ava.Source = myInfo.Ava;
            u_About.Text = myInfo.AboutMe;
            u_Nick.Content = myInfo.Nick;
        }
    }

    public class UserInformation
    {
        public string Nick { get; set; }
        public BitmapImage Ava { get; set; }
        public string AboutMe { get; set; }

        public UserInformation()
        {
            this.Nick = "Derpy";
            this.Ava = new BitmapImage(new Uri("/Ctrl_lib;component/Media/derp.jpg", UriKind.Relative));
            this.AboutMe = "Derp!!!";
        }

        public UserInformation(string _n, string _a, BitmapImage _i)
        {
            this.Nick = _n;
            this.Ava = _i;
            this.AboutMe = _a;
        }
    }
}
